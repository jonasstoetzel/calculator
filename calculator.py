#! /usr/bin/env python

'''
testlibrary für gitlab ci
'''


def add(first, second):
    return first + second


def subtract(first, second):
    return first - second


def multiply(first, second):
    return first * second
